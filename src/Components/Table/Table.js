import React from 'react'
import { usePagination, useSortBy, useTable } from 'react-table'
import PropTypes from 'prop-types'
import Button from '../Button/Button'
import { column } from '../../Data/column'
import './table.scss'

function Table({ tableData }) {
	const columns = React.useMemo(() => column, [column])
	const data = React.useMemo(() => tableData, [tableData])

	const {
		getTableProps,
		getTableBodyProps,
		headerGroups,
		page,
		nextPage,
		previousPage,
		canNextPage,
		canPreviousPage,
		prepareRow,
		pageOptions,
		state,
		setPageSize,
		gotoPage,
		pageCount,
	} = useTable(
		{
			columns,
			data,
			initialState: { pageSize: 4 },
		},
		useSortBy,
		usePagination
	)
	const { pageIndex, pageSize } = state

	return (
		<>
			<table {...getTableProps()}>
				<thead>
					{headerGroups.map((headerGroup, i) => (
						<tr key={i} {...headerGroup.getHeaderGroupProps()}>
							{headerGroup.headers.map((column, i) => (
								<th
									key={i}
									{...column.getHeaderProps(column.getSortByToggleProps())}
								>
									{column.render('Header')}

									<span>
										{column.isSorted
											? column.isSortedDesc
												? ' 🔽'
												: ' 🔼'
											: ''}
									</span>
								</th>
							))}
						</tr>
					))}
				</thead>
				<tbody {...getTableBodyProps()}>
					{page.map((row, i) => {
						prepareRow(row)
						return (
							<tr key={i} {...row.getRowProps()}>
								{row.cells.map((cell, i) => {
									return (
										<td {...cell.getCellProps()} key={i}>
											{cell.render('Cell')}
										</td>
									)
								})}
							</tr>
						)
					})}
				</tbody>
			</table>
			<div>
				<Button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
					◀◀
				</Button>
				<Button onClick={() => previousPage()} disabled={!canPreviousPage}>
					◀
				</Button>
				<Button>
					{pageIndex + 1} / {pageOptions.length}
				</Button>
				<Button onClick={() => nextPage()} disabled={!canNextPage}>
					▶
				</Button>
				<Button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
					▶▶
				</Button>
				<span style={{ marginLeft: '10px' }}>
					| Go to page:
					<input
						type='number'
						value={pageIndex + 1}
						onChange={(e) => {
							gotoPage(e.target.value ? Number(e.target.value) - 1 : 0)
						}}
						style={{ width: '40px', marginLeft: '5px', padding: '3px' }}
					/>
				</span>
				<span style={{ marginLeft: '20px' }}>
					post per page:
					<select
						value={pageSize}
						name='filter'
						id=''
						onChange={(e) => setPageSize(e.target.value)}
						style={{
							marginLeft: '5px',
							padding: '5px',
							border: '2px solid #000',
							borderRadius: '5px',
						}}
					>
						{[2, 4, 6, 8, 10].map((item, i) => (
							<option key={i} value={item}>
								{item}
							</option>
						))}
					</select>
				</span>
			</div>
		</>
	)
}
Table.propTypes = {
	tableData: PropTypes.any,
}
export default Table
