import React from 'react'
import PropTypes from 'prop-types'
import './button.scss'

function Button({ children, className, active, ...props }) {
	return (
		<button className={`btn ${active && 'active'} ${className} `} {...props}>
			{children}
		</button>
	)
}
Button.propTypes = {
	children: PropTypes.node,
	className: PropTypes.any,
	active: PropTypes.bool,
}
export default Button
