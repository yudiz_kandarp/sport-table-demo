import React from 'react'
import { format } from 'date-fns'
export const column = [
	{
		Header: 'Image',
		accessor: 'sImage',
		Cell: (tableProps) => (
			<img
				src={tableProps.row.original.sImage}
				width={60}
				alt={'sport Image'}
			/>
		),
	},
	{
		Header: 'Title',
		accessor: 'sTitle',
	},
	{
		Header: 'Description',
		accessor: 'sDescription',
	},
	{
		Header: 'Type',
		accessor: 'eType',
	},
	{
		Header: 'Created At',
		accessor: 'dCreatedAt',
		Cell: ({ value }) => {
			return format(new Date(value), 'dd/MM/yyyy')
		},
	},
	{
		Header: 'View Counts',
		accessor: 'nViewCounts',
	},
	{
		Header: 'Comments Count',
		accessor: 'nCommentsCount',
	},
]
